# QR Server android app

By Jan M (http://janm.si), Nov 12 2020

QR Server is a simple android app which scans a QR code, starts a minimal HTTP web server and displays
a content of the QR code on a local website. As long as your PC is connected to the same Wi-Fi network,
you can then access that website from your PC and copy the content. The HTTP server has 3 endpoints:

* http://yourLocalIp:3030/ - User-friendly page with QR code content, which is updated in real time.
* http://yourLocalIp:3030/raw - String content of last scanned QR code.
* http://yourLocalIp:3030/hash - Sha256 hash of last scanned QR code.

# Versions

Folder `./builds` should contain debug apk for each version:

| file                         | release date | notes                  |
|------------------------------|--------------|------------------------|
| `qr-server-v1.0-debug.apk`   | Nov 12, 2020 | First debug version.   |
| `qr-server-v1.0-release.apk` | Nov 13, 2020 | First release version. |

## Screenshots

<img src="./othr/readme/scan_sepa1.jpg" alt="Scan screen" width="250" />

<img src="./othr/readme/settings.jpg" alt="Settings screen" width="250" />

<img src="./othr/readme/client_sepa2.png" alt="Web client" width="250" />

## Legal documents

**License**

QR Server app is a free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3.0 of the
License, or (at your option) any later version.

QR Server app is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with software. If not, see <http://www.gnu.org/licenses/>.

**Other**

https://janm.si/other/qr-server/legal.php
