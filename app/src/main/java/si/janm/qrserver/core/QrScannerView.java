package si.janm.qrserver.core;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.Html;
import android.text.format.Formatter;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import si.janm.qrserver.R;
import com.google.zxing.Result;

import java.io.IOException;

import static android.content.Context.WIFI_SERVICE;

public class QrScannerView extends FrameLayout {

    private TextView textTitle;
    private TextView textMsg;
    private Button btnTryAgain;
    private ImageButton btnSettings;

    private WebServer webServer;
    private CodeScanner scanner;
    private String ip;
    private OnDecodeListener onDecodeListener;
    private SettingsDialog.OnSaveListener onSaveListener;


    /**
     * QR scanner view constructor.
     */
    public QrScannerView(@NonNull Context context) {
        super(context);
        this._QrScannerView();
    }
    public QrScannerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this._QrScannerView();
    }
    public QrScannerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this._QrScannerView();
    }
    public QrScannerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this._QrScannerView();
    }
    private void _QrScannerView(){
        inflate(this.getContext(), R.layout.view_qr_scanner, this);

        this.textTitle = findViewById(R.id.textTitle);
        this.textMsg = findViewById(R.id.textMsg);
        this.btnTryAgain = findViewById(R.id.btnTryAgain);
        this.btnSettings = findViewById(R.id.btnSettings);

        this.btnTryAgain.setOnClickListener(new OnTryAgainClick());
        this.btnSettings.setOnClickListener(new OnSettingsClick());
    }


    /**
     * Some setters.
     */
    public void setTitle(String title){
        this.textTitle.setText(title);
    }
    public void setWebServer(WebServer webServer){
        this.webServer = webServer;
    }
    public void setMessage(String msg){
        this.textMsg.setText(msg);
    }
    public void setOnDecodeListener(OnDecodeListener listener){
        this.onDecodeListener = listener;
    }
    public void setOnSaveListener(SettingsDialog.OnSaveListener listener){
        this.onSaveListener = listener;
    }


    /**
     * Start the server and scanner.
     */
    public void start(){

        // start the server
        try {
            this.stop();
            this.webServer.serve();
        } catch (IOException e) {
            e.printStackTrace();
            this.setMessage("Failed to start server: "+e.getMessage());
            this.showTryAgainBtn(true);
            return;
        }

        WifiManager wifiMgr = (WifiManager) this.getContext().getApplicationContext().getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        if (ip == 0) {
            this.setMessage("Failed to connect to Wi-Fi.");
            this.showTryAgainBtn(true);
            return;
        }
        this.ip = Formatter.formatIpAddress(ip);

        // start the scanner
        if (this.scanner == null) {
            CodeScannerView scannerView = findViewById(R.id.scanner);
            this.scanner = new CodeScanner(this.getContext(), scannerView);
            this.scanner.setDecodeCallback(new OnDecode());
            this.scanner.startPreview();
            scannerView.setOnClickListener(new OnScannerClick());
        }

        this.showTryAgainBtn(false);
        this.showOnlineMessage();
    }

    /**
     * Stop the server and scanner.
     */
    public void stop(){
        if (this.webServer.isAlive()) {
            this.webServer.stop();
        }
    }

    /**
     * Restart the server and scanner.
     */
    public void restart(){
        this.stop();
        this.start();
    }

    /**
     * Show or hide Try again / Settings button.
     */
    private void showTryAgainBtn(boolean show){
        if (show){
            this.btnTryAgain.setVisibility(VISIBLE);
        } else {
            this.btnTryAgain.setVisibility(GONE);
        }
    }


    /**
     * Show server is running message.
     */
    private void showOnlineMessage(){
        String link = String.format("http://%s:%d", this.ip, this.webServer.getListeningPort());
        String html = String.format("Server is running on: <a href=\"%s\">%s</a>", link, link);
        this.textMsg.setText(Html.fromHtml(html));
        this.textMsg.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * When QR code is decoded.
     */
    private class OnDecode implements DecodeCallback{
        @Override
        public void onDecoded(@NonNull Result result) {
            String text = result.getText();
            webServer.publish(text);

            if (onDecodeListener != null) {
                onDecodeListener.decoded(text);
            }
        }
    }

    /**
     * When clicked on QR code scanner.
     */
    private class OnScannerClick implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            if (scanner != null) {
                scanner.startPreview();
            }
        }
    }

    /**
     * When try again button is clicked.
     */
    private class OnTryAgainClick implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            restart();
        }
    }

    /**
     * When settings button is clicked.
     */
    private class OnSettingsClick implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            SettingsDialog dialog = new SettingsDialog(getContext());
            dialog.setOnSaveListener(onSaveListener);
            dialog.show();
        }
    }

    /**
     * Helper callback classes.
     */
    public static abstract class OnDecodeListener {
        public abstract void decoded(String str);
    }

}
