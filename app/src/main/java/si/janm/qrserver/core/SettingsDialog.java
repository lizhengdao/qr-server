package si.janm.qrserver.core;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import si.janm.qrserver.R;
import si.janm.qrserver.TheApplication;

public class SettingsDialog extends AlertDialog {

    private View view;

    private EditText inputPort;
    private OnSaveListener onSaveListener;

    /**
     * Settings dialog constructor.
     */
    public SettingsDialog(@NonNull Context context) {
        super(context);
        this._SettingsDialog();
    }
    public SettingsDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        this._SettingsDialog();
    }
    protected SettingsDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this._SettingsDialog();
    }
    private void _SettingsDialog(){
        this.view = View.inflate(getContext(), R.layout.dialog_settings, null);

        this.inputPort = this.view.findViewById(R.id.inputPort);

        this.setTitle("Settings");
        this.setButton(Dialog.BUTTON_POSITIVE, "Save", new VoidClickListener());
        this.initAbout();
        this.setView(this.view);
    }


    /**
     * Some setters.
     */
    public void setOnSaveListener(OnSaveListener listener){
        this.onSaveListener = listener;
    }


    /**
     * On dialog open.
     */
    @Override
    public void show() {
        // load settings
        AppSettings settings = TheApplication.getInstance().getSettings();
        if (settings.port > 0){
            this.inputPort.setText(""+settings.port);
        }

        super.show();
        this.getButton(Dialog.BUTTON_POSITIVE).setOnClickListener(new OnSaveClick());
    }

    /**
     * Initialise dialog about section.
     */
    private void initAbout(){
        String appName = getContext().getResources().getText(R.string.app_name).toString();
        String versionName = TheApplication.getInstance().getPackageInfo().versionName;
        String link = "https://janm.si";
        String authorHtml = String.format("By Jan M - <a href=\"%s\">%s</a>", link, link);

        TextView textAppVersion = this.view.findViewById(R.id.appVersion);
        textAppVersion.setText(String.format("%s v%s", appName, versionName));

        TextView textAppAuthor = this.view.findViewById(R.id.appAuthor);
        textAppAuthor.setText(Html.fromHtml(authorHtml));
        textAppAuthor.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * On save button click
     */
    private class OnSaveClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int port = Utl.tryParseInt(inputPort.getText().toString(), -1);

            // validate input
            if (port < 10 || port > 99999){
                Utl.toast("Invalid port number.");
                return;
            }

            // save settings
            AppSettings settings = new AppSettings();
            settings.port = port;
            TheApplication.getInstance().saveSettings(settings);

            // save
            dismiss();
            if (onSaveListener != null) onSaveListener.onSave(settings);
        }
    }

    /**
     * Just do nothing
     */
    private static class VoidClickListener implements OnClickListener{
        @Override
        public void onClick(DialogInterface dialog, int which) {}
    }

    /**
     * Helper event listeners.
     */
    public static abstract class OnSaveListener{
        public abstract void onSave(AppSettings settings);
    }

}
