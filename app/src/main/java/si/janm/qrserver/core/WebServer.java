package si.janm.qrserver.core;

import si.janm.qrserver.TheApplication;

import java.io.IOException;
import java.io.InputStream;

import fi.iki.elonen.NanoHTTPD;

public class WebServer extends NanoHTTPD {

    private String text;
    private String hash;
    private String template;

    /**
     * Http server for QR code scanner.
     *
     * @param port: listen on port.
     */
    public WebServer(int port) {
        super(port);

        this.text = "";
        this.hash = "";
        this.template = "";

        // read preview.html template
        try {
            InputStream is = TheApplication.getInstance().getAppContext().getAssets().open("display.html");
            this.template = Utl.readIs(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Start the server on specified port.
     */
    public void serve() throws IOException {
        this.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
    }

    /**
     * Main server request handler.
     */
    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();

        switch (uri){
            case "/":
                return this.handleRoot();
            case "/raw":
                return this.handleRaw();
            case "/hash":
                return this.handleHash();
        }

        return newFixedLengthResponse("");
    }

    /**
     * Set content to display.
     */
    public void publish(String text) {
        this.hash = Utl.sha256(text);
        this.text = text;
    }

    /**
     * Handle endpoint /
     */
    private Response handleRoot(){
        String html = this.template.replace("[content]", text);
        return newFixedLengthResponse(html);
    }

    /**
     * Handle endpoint /raw
     */
    private Response handleRaw(){
        NanoHTTPD.Response res = newFixedLengthResponse(this.text);
        res.addHeader("Content-Type", "text/plain");
        return res;
    }

    /**
     * Handle endpoint /hash
     */
    private Response handleHash(){
        NanoHTTPD.Response res = newFixedLengthResponse(this.hash);
        res.addHeader("Content-Type", "text/plain");
        return res;
    }

}
